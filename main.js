$(document).ready(function() {
  const $button = $('.btn-change-theme');
  const style = document.documentElement.style;
  const light = 'light';
  const dark = 'dark';

  const setColorScheme = (theme) => {
    style.setProperty('--color1', themes[theme]['--color1']);
    style.setProperty('--color2', themes[theme]['--color2']);
    style.setProperty('--color3', themes[theme]['--color3']);
  };

  const setTheme = (theme) => {
    if (themes.hasOwnProperty(theme)) {
      setColorScheme(theme);
      localStorage.setItem(`theme`, theme);
    }
  };

  const getTheme = () => {
    const theme = localStorage.getItem('theme') || light;
    setTheme(theme);
  };

  getTheme();

  $button.on('click', function() {
    const theme = localStorage.getItem('theme');
    if (theme === light) {
      setTheme(dark);
    } else {
      setTheme(light);
    }
  });
});