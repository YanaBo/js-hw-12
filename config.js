const themes = {
  light: {
    '--color1': 'rgba(0, 0, 0, 0.5)',
    '--color2': '#3cb878',
    '--color3': 'white',
  },
  dark: {
    '--color1': 'rgba(0, 0, 0, 0.9)',
    '--color2': '#D53E3E',
    '--color3': 'rgba(0, 0, 0, 0.7)',
  },
};
